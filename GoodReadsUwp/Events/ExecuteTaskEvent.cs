﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Events
{
    //class ExecuteTaskEvent : PubSubEvent<Tuple<Func<Task> , Func<Task>, Action>>
    class ExecuteTaskEvent : PubSubEvent<Tuple<Func<Task>, Func<Task>, Action>>
    {

    }
}
