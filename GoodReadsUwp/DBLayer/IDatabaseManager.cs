﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.DBLayer
{
    public interface IDatabaseManager
    {
        SQLiteConnection dbConnection();

        void closeDBConnection();

        bool Savelist(IEnumerable<Object> list);

        bool Save(Object item);


    }
}
