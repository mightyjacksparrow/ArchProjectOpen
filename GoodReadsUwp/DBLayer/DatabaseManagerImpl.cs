﻿using GoodReadsUwp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace GoodReadsUwp.DBLayer
{
    /// <summary>
    /// Class that facilitates all CRUD Operations.
    /// http://rbdocumentation.com/html/403dfe8a-d222-dade-406b-179e5acdd7c0.htm
    /// </summary>
    public class DatabaseManagerImpl : IDatabaseManager
    {
        private static SQLiteConnection _connection;
        private static object _lockObj = new object();
        private static string DB_PATH = Path.Combine(ApplicationData.Current.LocalFolder.Path, "db.sqlite");

        public DatabaseManagerImpl()
        {
            dbConnection();
            dbConnection().CreateTable<MailModel>();
           // dbConnection().DeleteAll<MailModel>();
        }

        public SQLiteConnection dbConnection()
        {
            if (_connection == null)
            {
                try
                {
                    lock (_lockObj)
                    {
                        if (_connection == null)
                        {
                            _connection = new SQLiteConnection(DB_PATH);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return _connection;
        }

        public void closeDBConnection()
        {
            if (_connection != null)
            {
                lock (_lockObj)
                {
                    if (_connection != null)
                    {
                        _connection.Close();
                        _connection = null;
                    }
                }
            }
        }

        public bool Savelist(IEnumerable<object> list)
        {
            bool r = _connection.InsertAll(list) > 0 ? true: false ;
            return r;
        }

        public bool Save(object item)
        {
            bool r = _connection.Insert(item) > 0 ? true : false;
            return r;
        }
    }
}
