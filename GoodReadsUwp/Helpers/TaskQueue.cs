﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GoodReadsUwp.Helpers
{
    class TaskQueue
    {
        private SemaphoreSlim semaphore;// = new SemaphoreSlim(4);
        public TimeSpan TimeBetweenTaskExecutions { get; set; }
        public int NumberOfRetries { get; set; }
       // public int noOfThreads = 1;

        public TaskQueue(int threadCount)
        {
            semaphore= new SemaphoreSlim(threadCount);
        }

        public async Task<T> Enqueue<T>(Func<Task<T>> taskGenerator)
        {
            await semaphore.WaitAsync();

            int numberOfTriesRemaining = NumberOfRetries + 1;
            Task delay = null;
            try
            {
                Func<Task<T>> wrappedGenerator = () =>
                {
                    delay = Task.Delay(TimeBetweenTaskExecutions);
                    return taskGenerator();
                };
                return await TaskUtilities.RetryOnFailure(wrappedGenerator, NumberOfRetries, TimeBetweenTaskExecutions)
                    .ConfigureAwait(false);
            }
            finally
            {
                ReleaseAfterDelay(delay);
            }
        }

        public Task Enqueue(Func<Task> taskGenerator)
        {
            return Enqueue(() => TaskUtilities.WithResult(taskGenerator(), true));
        }

        private async void ReleaseAfterDelay(Task delay)
        {
            try
            {
                await delay
                        .ConfigureAwait(false);
            }
            finally
            {
                semaphore.Release();
            }
        }
    }

    public static class TaskUtilities
    {
        public static async Task<T> WithResult<T>(Task task, T value)
        {
            await task.ConfigureAwait(false);
            return value;
        }

        public static async Task<T> RetryOnFailure<T>(Func<Task<T>> taskGenerator,
            int numberOfRetries = 1, TimeSpan? timeBetweenExecutions = null)
        {
            int numberOfTriesRemaining = numberOfRetries + 1;
            while (true)
            {
                var delayTask = Task.Delay(timeBetweenExecutions ?? TimeSpan.Zero);
                try
                {
                    return await taskGenerator();
                }
                catch
                {
                    numberOfTriesRemaining--;
                    if (numberOfTriesRemaining == 0)
                        throw;
                    await delayTask;
                }
            }
        }
    }
}

