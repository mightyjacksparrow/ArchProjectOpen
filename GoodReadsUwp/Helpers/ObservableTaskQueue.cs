﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GoodReadsUwp.Helpers
{
    class ObservableTaskQueue : INotifyPropertyChanged
    {
        public event Action StartWorking;
        public event Action StopWorking;
        public event PropertyChangedEventHandler PropertyChanged;
        public static int threadCount = 1;

        private TaskQueue queue;// = new TaskQueue();
        
        private int tasksQueuedSinceQueueWasLastIdle = 0;
        private int pendingTasks = 0;
        public int Progress
        {
            get
            {
                if (tasksQueuedSinceQueueWasLastIdle == 0)
                    return 0;

                int numberOfCompletedTasks = tasksQueuedSinceQueueWasLastIdle - pendingTasks;
                return numberOfCompletedTasks * 100 / tasksQueuedSinceQueueWasLastIdle;
            }
        }
        public TimeSpan TimeBetweenTaskExecutions
        {
            get { return queue.TimeBetweenTaskExecutions; }
            set { queue.TimeBetweenTaskExecutions = value; }
        }
        public int NumberOfRetries
        {
            get { return queue.NumberOfRetries; }
            set { queue.NumberOfRetries = value; }
        }

        public ObservableTaskQueue(int threadCount)
        {
            queue = new TaskQueue(threadCount);
        }

        public async Task<T> Enqueue<T>(Func<Task<T>> taskGenerator)
        {
            Interlocked.Increment(ref tasksQueuedSinceQueueWasLastIdle);
            if (Interlocked.Increment(ref pendingTasks) == 1)
                StartWorking?.Invoke();
            NotifyPropertyChanged("Progress");
            try
            {
                return await queue.Enqueue(taskGenerator).ConfigureAwait(false);
            }
            finally
            {
                NotifyPropertyChanged("Progress");
                if (Interlocked.Decrement(ref pendingTasks) == 0)
                {
                    Interlocked.Exchange(ref tasksQueuedSinceQueueWasLastIdle, 0);
                    StopWorking?.Invoke();
                }
            }
        }

        public Task Enqueue(Func<Task> taskGenerator)
        {
            return Enqueue(() => TaskUtilities.WithResult(taskGenerator(), true));
        }

        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

