﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace GoodReads.Models
{
    [XmlRoot(ElementName = "results")]
    public class Results
    {
        [XmlElement(ElementName = "work")]
        public List<Work> Work { get; set; }
    }
}
