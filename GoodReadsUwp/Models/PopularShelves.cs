﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace GoodReads.Models
{
    [XmlRoot(ElementName = "popular_shelves")]
    public class PopularShelves
    {
        [XmlElement(ElementName = "shelf")]
        public List<Shelf> Shelf { get; set; }
    }
}
