﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Models
{
    public class MailModel
    {
        public string Subject { get; set; }
      
        public string Sender { get; set; }

        public string Content { get; set; }

        public string isFlagged { get; set; }

    }
}
