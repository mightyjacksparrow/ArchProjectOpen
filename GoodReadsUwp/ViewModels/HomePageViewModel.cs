﻿using GoodReadsUwp.DomainViewModels;
using GoodReadsUwp.Events;
using GoodReadsUwp.Models;
using GoodReadsUwp.Services;
using Prism.Commands;
using Prism.Events;
using Prism.Windows.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace GoodReadsUwp.ViewModels
{
    public class HomePageViewModel :  ViewModelBase
    {
        public ICollection<MailItemViewModel> _Testlist;
        private readonly IEventAggregator _eventAggregator;
        private IExecutor _executor;
        private IGoodReadService _goodReadService;
        public ICommand clickCommand { get; private set; }

        public ICollection<MailItemViewModel> Testlist
        {
            get { return _Testlist; }
            private set { SetProperty(ref _Testlist, value); }
        }

        public HomePageViewModel(IEventAggregator eventAggregator, IExecutor executor, IGoodReadService goodReadService)
        {
            _eventAggregator = eventAggregator;
            _executor = executor;
            _goodReadService = goodReadService;
            clickCommand = DelegateCommand.FromAsyncHandler(clickEvent);
            Testlist = new ObservableCollection<MailItemViewModel>();
            for (int a = 0; a < 1; a++)
            {
                MailModel m = new MailModel();
                m.Sender = "Number " + a;
                m.Subject = "Sub " + a + " ject";
                Testlist.Add(new MailItemViewModel(m));
            }

            if (_eventAggregator != null)
            {
                _eventAggregator.GetEvent<DBSaveServiceEvent>().Subscribe(UpdateDBValues);
                _eventAggregator.GetEvent<MailAddedEvent>().Subscribe(UpdateMailList);
                
            }
        }

        private async Task clickEvent()
        {
            //Trigger Test Event to Add 150 mails to DB
            List<MailModel> aa = new List<MailModel>();
            Random r = new Random();
            for (int a = 0; a < 1; a++)
            {
                MailModel m = new MailModel();
                m.Sender = "AAAA " + a + " " +(r.Next()).ToString();
                m.Subject = "BBB " + a + " RR";
                aa.Add(m);
               // Testlist.Add(new MailItemViewModel(m));
            }
            _goodReadService.addMails(aa);
            //Just Trigger the Event
            var c =  _goodReadService.getAllItems();
        }

        private void UpdateDBValues()
        {
            //Event Triggered After Values are Updated in DB(DBSaveServiceEvent)
            var c = Observable.Start(update);
        }

        private void UpdateMailList()
        {
            var c = Observable.Start(getMails);
        }

        private async Task getMails()
        {
            var c = new ObservableCollection<MailModel>(_goodReadService.getAllMails().Result.ToList());
            List<MailItemViewModel> g = new List<MailItemViewModel>();
            foreach(MailModel b in c)
            {
                g.Add(new MailItemViewModel(b));
            }
        
            //Do All the Manipulation Stuff in Background Worker Threads!
            //Access UI thread only to Update the Final Value to UI
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => Testlist.Add(g.FirstOrDefault()));
            //await CoreApplication.MainView.CoreWindow.Dispatcher.RunIdleAsync(() => Testlist = g);
        }


        private async Task update()
        {
            //Event Execution in Seperate Thread, Will Update UI once when Fetched fronm DB
            //var c = new ObservableCollection<String>(_goodReadService.upp().Result.ToList());
            //await   CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => Testlist = c);
        }

    }
}
