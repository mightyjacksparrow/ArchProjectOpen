﻿using GoodReadsUwp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Services
{
    class NetworkQueueService : INetworkQueue
    {
        ObservableTaskQueue networkTaskQueue;

        public NetworkQueueService()
        {
            networkTaskQueue = new ObservableTaskQueue(4);
        }

        public Task Enqueue(Func<Task> taskGenerator)
        {
            return networkTaskQueue.Enqueue(taskGenerator);
        }

        public Task<T> Enqueue<T>(Func<Task<T>> taskGenerator)
        {
            return networkTaskQueue.Enqueue(taskGenerator);
        }
    }
}
