﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Services
{
    public interface IDBQueue
    {
        Task<T> Enqueue<T>(Func<Task<T>> taskGenerator);
        Task Enqueue(Func<Task> taskGenerator);
    }
}
