﻿using GoodReadsUwp.Events;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;

namespace GoodReadsUwp.Services
{
    class ExecutorService : IExecutor
    {
        private readonly IEventAggregator _eventAggregator;
        private INetworkQueue _networkQueue;
        private IDBQueue _dbQueue;
        Tuple<Func<Task>, Func<Task>,Action> passedValue ;

        public ExecutorService(IEventAggregator eventAggregator, INetworkQueue networkQueue , IDBQueue dbQueue)
        {
            _eventAggregator = eventAggregator;
            _networkQueue = networkQueue;
            _dbQueue = dbQueue;
            if (_eventAggregator != null)
            {
                _eventAggregator.GetEvent<ExecuteTaskEvent>().Subscribe(executeTaskAsync);
            }
        }

        /// <summary>
        /// Executes the Given Tasks, by Adding DB and Network Tasks to Seperate Queues, Executing them in Parallel
        /// </summary>
        /// <param name="obj"></param>
        public void executeTaskAsync(Tuple<Func<Task>, Func<Task>, Action> obj)
        {
            passedValue = obj;

            Debug.WriteLine("ExecuteTask Called from :" + CoreApplication.MainView.CoreWindow.Dispatcher.HasThreadAccess);

            //Add to Db Queue ASynchronously. It Works on the Same Thread I Guess
            //The Callback Command will be Triggered After Completion of DBSave.
            Observable.Start(addToDB)
                      .Finally(obj.Item3)
                      .Subscribe();

            //Add to Network Queue
            Observable.Start(syncNet);
        }


        /// <summary>
        /// Adds Task to be Executed on DB
        /// </summary>
        /// <returns></returns>
        public async Task addToDB()
        {
            await _dbQueue.Enqueue(passedValue.Item1);
        }

        /// <summary>
        /// Adds Task to be Executed over Network
        /// </summary>
        /// <returns></returns>
        public async Task syncNet()
        {
            await _networkQueue.Enqueue(passedValue.Item2);
        }

    }
}
