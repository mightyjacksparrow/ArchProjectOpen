﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Services
{
    public interface IExecutor
    {
        void executeTaskAsync(Tuple<Func<Task>, Func<Task>, Action> obj);

    }
}
