﻿using GoodReadsUwp.DBLayer;
using GoodReadsUwp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Services
{
    /// <summary>
    /// Class that Forms an Abstract Layer Between Database and Services
    /// All calls From and To Db will be Through this Class
    /// </summary>
    public class PersistenceHelper : IPersistenceHelper
    {
        private IDatabaseManager _databaseManager;

        public PersistenceHelper(IDatabaseManager databaseManager)
        {
            _databaseManager = databaseManager;
        }

        public Task<List<String>> getAllItems()
        {
            List<String> result = new List<String>();
            for(int a = 0; a < 100; a++)
            {
                result.Add(a.ToString());
            }

            return Task.FromResult(result);
        }

        public Task<List<MailModel>> getAllmails()
        {
            TableQuery<MailModel> c = from p in _databaseManager.dbConnection().Table<MailModel>() select p ;
            return Task.FromResult(new List<MailModel>(c));
        }

        public Task<string> getItem()
        {
            throw new NotImplementedException();
        }

        public void saveMails(List<MailModel> input)
        {
            _databaseManager.Savelist(input);
        }
    }
}
