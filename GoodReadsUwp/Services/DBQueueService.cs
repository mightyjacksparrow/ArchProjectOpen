﻿using GoodReadsUwp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Services
{
    public class DBQueueService : IDBQueue
    {
        ObservableTaskQueue dbTaskQueue;

        public DBQueueService()
        {
            dbTaskQueue = new ObservableTaskQueue(1);
        }

        public Task Enqueue(Func<Task> taskGenerator)
        {
           return dbTaskQueue.Enqueue(taskGenerator);
        }

        public Task<T> Enqueue<T>(Func<Task<T>> taskGenerator)
        {
            return dbTaskQueue.Enqueue(taskGenerator);
        }
    }
}