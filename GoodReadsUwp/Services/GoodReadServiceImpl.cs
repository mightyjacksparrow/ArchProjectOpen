﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodReads.Models;
using GoodReadsUwp.DBLayer;
using Prism.Events;
using GoodReadsUwp.Events;
using System.Diagnostics;
using GoodReadsUwp.Models;
using Windows.ApplicationModel.Core;

namespace GoodReadsUwp.Services
{
    public class GoodReadServiceImpl : IGoodReadService
    {
        private readonly IEventAggregator _eventAggregator;
        private IPersistenceHelper _persistenceHelper;
        private ISyncHelper _syncHelper;
        private IExecutor _executor;

        public GoodReadServiceImpl(IEventAggregator eventAggregator,IPersistenceHelper persistenceHelper, ISyncHelper syncHelper,IExecutor executor)
        {
            _persistenceHelper = persistenceHelper;
            _syncHelper = syncHelper;
            _executor = executor;
            _eventAggregator = eventAggregator;
        }

        public Task<List<string>> getAllItems()
        {
            //Trigger the Event
        //   _eventAggregator.GetEvent<ExecuteTaskEvent>().Publish(new Tuple<Func<Task>, Func<Task>, Action>(getAllItemsDBTask, getAllItemsNetTask, dbCallback));

            //Returns Nothing As of Now
            return Task.FromResult(new List<String>());
        }

        private Task<List<String>> getAllItemsDBTask()
        {
            List<String> res = new List<String>();
            for(int a = 0; a < 000; a++)
            {
                res.Add("!!!!!!!");
            }
            foreach(String f in res)
            {
                Debug.WriteLine(f);
            }

            return Task.FromResult(res);
        }

        private Task getAllItemsNetTask()
        {
            List<String> res = new List<String>();
            for (int a = 0; a < 7000; a++)
            {
                res.Add(a.ToString());
            }
            foreach (String f in res)
            {
                Debug.WriteLine(f);
            }
            return Task.FromResult(res);
        }

        private void dbCallback()
        {
            //Wama
            raiseDBServiceEvent();
            Debug.WriteLine("Mudinjadu Ba!!!");
        }

        private void raiseDBServiceEvent()
        {
            _eventAggregator.GetEvent<DBSaveServiceEvent>().Publish();
        }

        public Task<string> getItem()
        {
            throw new NotImplementedException();
        }

        public Task<Search> searchBooks(string query)
        {
            if(String.IsNullOrEmpty(query)){
                return null;
            }

            //Should Fetch From DB or Network
            return null;
        }

        public Task<List<string>> upp()
        {
            List<String> result = new List<String>();
            for (int a = 0; a < 100; a++)
            {
                result.Add(a.ToString()+"---");
            }
            return Task.FromResult(result);
        }

        List<MailModel> tobeSaved;
        public void addMails(List<MailModel> input)
        {
            Debug.WriteLine("addMails Called from :" + CoreApplication.MainView.CoreWindow.Dispatcher.HasThreadAccess);

            Random r = new Random();
            for(int ra =0;ra<25;ra++)
            {
                MailModel m = new MailModel();
                m.Sender = "AAAA " + ra + " " + (r.Next()).ToString();
                m.Subject = "BBB " + ra + " RR";
                input.Add(m);
                // Testlist.Add(new MailItemViewModel(m));
            }

            tobeSaved = input;
            //Here You Have Done with Events! But this can Also Be Done with Callbacks
            //All 3 Events are Fire and Forget!

            //Instead of Task, Pass Delegates, That Will Allow You to Pass Parameters too with!
            _eventAggregator.GetEvent<ExecuteTaskEvent>().Publish(new Tuple<Func<Task>, Func<Task>, Action>(createMails, syncWithNet, afterSaveCallback));
        }


        private async Task createMails()
        {
            _persistenceHelper.saveMails(tobeSaved);
        }

        private async Task syncWithNet()
        {

        }

        private void afterSaveCallback()
        {
            _eventAggregator.GetEvent<MailAddedEvent>().Publish();
        }

        public Task<List<MailModel>> getAllMails()
        {
            return _persistenceHelper.getAllmails();
        }
    }
}
