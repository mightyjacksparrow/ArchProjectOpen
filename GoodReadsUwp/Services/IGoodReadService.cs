﻿using GoodReads.Models;
using GoodReadsUwp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Services
{
    public interface IGoodReadService
    {
        Task<Search> searchBooks(String query);

        Task<String> getItem();

        Task<List<String>> getAllItems();

        Task<List<String>> upp();

        void addMails(List<MailModel> input);

        Task<List<MailModel>> getAllMails();
    }
}
