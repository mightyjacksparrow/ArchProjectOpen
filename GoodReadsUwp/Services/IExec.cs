﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Services
{
    public interface IExec
    {
        void executeKaro(Task t, bool isNetwork);

        void executeKaro(Task dbTask, Task feedBackTask);

        void executeKaro(Task networkTask, Task dbtask, Task feedBackTask);
    }
}
