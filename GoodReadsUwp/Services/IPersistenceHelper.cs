﻿using GoodReadsUwp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.Services
{
    public interface IPersistenceHelper 
    {
        Task<String> getItem();

        Task<List<String>> getAllItems();

        void saveMails(List<MailModel> input);

        Task<List<MailModel>> getAllmails();
    }
}
