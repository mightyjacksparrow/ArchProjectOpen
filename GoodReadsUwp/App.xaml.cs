﻿
using GoodReadsUwp.DBLayer;
using GoodReadsUwp.Services;
using Microsoft.Practices.Unity;
using Prism.Mvvm;
using Prism.Events;
using Prism.Unity.Windows;
using Prism.Windows.AppModel;
using Prism.Windows.Navigation;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using System.Globalization;
using System;
using Windows.ApplicationModel.Resources;

namespace GoodReadsUwp
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : PrismUnityApplication
    {
        // Bootstrap: App singleton service declarations
        public IEventAggregator EventAggregator { get; set; }

        /// <summary>
        /// Will be Loaded First
        /// Set up the list of known types for the SuspensionManager
        /// Override the OnRegisterKnownTypesForSerialization method in the App class to register any non-primitive types that need to be saved and restored to survive app termination.
        /// </summary>
        protected override void OnRegisterKnownTypesForSerialization()
        {
            // Set up the list of known types for the SuspensionManager
            // Override the OnRegisterKnownTypesForSerialization method in the App class to register any non-primitive types that need to be saved and restored to survive app termination.

            //SessionStateService.RegisterKnownType(typeof(UserModel));
        }

        /// <summary>
        /// Will be Loaded Second
        /// </summary>
        protected override Task OnInitializeAsync(IActivatedEventArgs args)
        {
            //Registering infrastructure services.
            //Registering types and instances that you use in constructors.
            //Providing a delegate that returns a view model type for a given view type.
            EventAggregator = new EventAggregator();

            //Register Instacnes
            //This code registers service instances with the container as singletons, based on their respective interfaces, so that the view model classes can take dependencies on them.
            //This means that the container will cache the instances on behalf of the app, with the lifetime of the instances then being tied to the lifetime of the container.
            Container.RegisterInstance<INavigationService>(NavigationService);
            Container.RegisterInstance<ISessionStateService>(SessionStateService);
            Container.RegisterInstance<IEventAggregator>(EventAggregator);

            //To Load Resources from Resources File
         //   Container.RegisterInstance<IResourceLoader>(new ResourceLoaderAdapter(new ResourceLoader()));

            //Register Services so that Container can Identify it and do the Honours
            Container.RegisterType<IAccountService, AccountServiceImpl>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IGoodReadService, GoodReadServiceImpl>(new ContainerControlledLifetimeManager());

            //Register DB layer Service
            Container.RegisterType<IDatabaseManager, DatabaseManagerImpl>(new ContainerControlledLifetimeManager());

            //For Api Calls
            Container.RegisterType<IApiService, ApiServiceImpl>(new ContainerControlledLifetimeManager());

            //QueueServices
            Container.RegisterType<INetworkQueue, NetworkQueueService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IDBQueue, DBQueueService>(new ContainerControlledLifetimeManager());

            //Add Executor
            Container.RegisterType<IExecutor, ExecutorService>(new ContainerControlledLifetimeManager());

            //Data
            Container.RegisterType<ISyncHelper, SyncHelper>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IPersistenceHelper, PersistenceHelper>(new ContainerControlledLifetimeManager());


            //For ViewModel Locator
            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver((viewType) =>
            {
                //Get AssemblyInfo From Project properties and Add it Here!
                var viewModelTypeName = string.Format(CultureInfo.InvariantCulture, "GoodReadsUwp.ViewModels.{0}ViewModel, GoodReadsUwp, Version=1.0.0.0, Culture=neutral", viewType.Name);
                var viewModelType = Type.GetType(viewModelTypeName);
                return viewModelType;
            });

            //Instantiate DBManager Static Class.

            return base.OnInitializeAsync(args);
        }

        /// <summary>
        /// Will be Loaded Last
        /// </summary>
        protected override Task OnLaunchApplicationAsync(LaunchActivatedEventArgs args)
        {
            NavigationService.Navigate("Home", null);

            //The OnLaunchApplication method returns a Task, allowing it to launch a long running operation. If you don't have a long running operation to launch you should return an empty Task.
            return Task.FromResult<object>(null);
        }
    }
}
