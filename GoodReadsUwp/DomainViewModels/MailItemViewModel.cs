﻿using GoodReadsUwp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsUwp.DomainViewModels
{
    public class MailItemViewModel
    {
        private readonly MailModel _mailModel;

        public MailItemViewModel(MailModel mailModel)
        {
            _mailModel = mailModel;
        }

        public string Subject
        {
            get { return _mailModel.Subject; }
        }

        public string Sender
        {
            get { return _mailModel.Sender; }
        }

        public bool isFlagged
        {
            get { return _mailModel.isFlagged == "0" ? true : false; }
        }


    }
}
