A Simple Project to Demonstrate Event Driven Programming, Architectured with MVVM, Prism, Rx.Net.


Brief intro About the App:
Has Views, ViewModels, Models, Services(I call them as Collection of UseCases in UWp Context) and other Helper Classes.

General App Flow:
Scenario: On Click of a Button, Add a Bunch of items to a Table, Retrieve and show the Updated List in UI.

From View, a Button is Clicked --> 
The command is Processed in ViewModel --> 
Viewmodel calls an API in Service (Service Defines what has to be exectued in DB task and What has to be executed in Network Task).
Service Publishes an ExecutorEvent which will be Subscribed by a Seperate Executorservice class which Handles the Execution).
DB and Network task are executed in Executorservice class with Seperate Threads -->
On Succesful Write in DB, an Event is Triggered in Service Layer(from callback), to Notify ViewModel(s) -->
ViewModel Fetches the Updated Value From Db in a Seperate Thread and Updates the Field in ViewModel, which in Turn Updates the View!


